﻿using Assignment_1.Graph;
using Assignment_1.Puzzle;
using Assignment_1.Searches;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    class Program
    {

        private static void OutputSearchResult<T>(Search<T> s)
        {
            Console.WriteLine("--------------------");
            Console.WriteLine("Time elapsed : {0:ss\\.ffffff}s", s.LastSearchTime);
            Console.WriteLine("Path cost : {0}", s.Path.Cost);
            Console.WriteLine("Path size : {0} (From {1} to {2})", s.Path.Size, s.Path.FirstNode, s.Path.LastNode);
            Console.WriteLine("--------------------");
        }

        static void Main(string[] args)
        {

            Node<string> A = new Node<string>("A");
            Node<string> B = new Node<string>("B");
            Node<string> C = new Node<string>("C");
            Node<string> D = new Node<string>("D");
            Node<string> E = new Node<string>("E");
            Node<string> F = new Node<string>("F");
            Node<string> G = new Node<string>("G");
            Node<string> H = new Node<string>("H");
            Node<string> I = new Node<string>("I");
            Node<string> J = new Node<string>("J");
            Node<string> K = new Node<string>("K");
            Node<string> L = new Node<string>("L");
            Node<string> M = new Node<string>("M");

            A.AddSiblingUndirected(B, 0);
            A.AddSiblingUndirected(C, 0);
            B.AddSiblingUndirected(D, 0);
            B.AddSiblingUndirected(E, 0);
            E.AddSiblingUndirected(F, 0);
            E.AddSiblingUndirected(G, 0);
            G.AddSiblingUndirected(H, 0);
            G.AddSiblingUndirected(I, 0);
            H.AddSiblingUndirected(J, 0);
            H.AddSiblingUndirected(K, 0);
            J.AddSiblingUndirected(L, 0);
            J.AddSiblingUndirected(M, 0);

            Console.WriteLine("----- Depth First Search ---------------");
            Search<string> dfs = new DepthFirstSearch<string>(A);
            if (dfs.FindGoal(C, true))
            {
                OutputSearchResult(dfs);   
            }

            Console.WriteLine();

            Console.WriteLine("----- Breadth First Search ---------------");
            Search<string> bfs = new BreadthFirstSearch<string>(A);
            if (bfs.FindGoal(C, true))
            {
                OutputSearchResult(bfs);
            }

            Node<string> n1 = new FixedHeuristicNode<string>("Arad", 366);
            Node<string> n2 = new FixedHeuristicNode<string>("Bucharest", 0);
            Node<string> n3 = new FixedHeuristicNode<string>("Craiova", 160);
            Node<string> n4 = new FixedHeuristicNode<string>("Dobreta", 366);
            Node<string> n5 = new FixedHeuristicNode<string>("Fagaras", 176);
            Node<string> n6 = new FixedHeuristicNode<string>("Oradea", 380);
            Node<string> n7 = new FixedHeuristicNode<string>("Lugoj", 244);
            Node<string> n8 = new FixedHeuristicNode<string>("Mehadia", 241);
            Node<string> n9 = new FixedHeuristicNode<string>("Pitesti", 100);
            Node<string> n10 = new FixedHeuristicNode<string>("Rimnicu Vilcea", 193);
            Node<string> n12 = new FixedHeuristicNode<string>("Sibiu", 253);
            Node<string> n13 = new FixedHeuristicNode<string>("Timisoara", 329);
            Node<string> n14 = new FixedHeuristicNode<string>("Zerind", 374);

            n1.AddSiblingUndirected(n13, 118);
            n1.AddSiblingUndirected(n12, 140);
            n1.AddSiblingUndirected(n14, 75);

            n13.AddSiblingUndirected(n7, 111);

            n7.AddSiblingUndirected(n8, 70);

            n8.AddSiblingUndirected(n4, 75);

            n4.AddSiblingUndirected(n3, 120);

            n3.AddSiblingUndirected(n9, 138);
            n3.AddSiblingUndirected(n10, 146);

            n10.AddSiblingUndirected(n9, 97);
            n10.AddSiblingUndirected(n12, 80);

            n14.AddSiblingUndirected(n6, 71);

            n6.AddSiblingUndirected(n12, 151);

            n12.AddSiblingUndirected(n5, 99);

            n5.AddSiblingUndirected(n2, 211);

            n9.AddSiblingUndirected(n2, 101);

            Console.WriteLine();
            Console.WriteLine("----- Best First Search ---------------");
            Search<string> bestFs = new BestFirstSearch<string>(n1);
            if (bestFs.FindGoal(n2, true))
            {
                OutputSearchResult(bestFs);
                Console.WriteLine("Path : ");
                Console.WriteLine(bestFs.Path);
                Console.WriteLine("---------------------------------------");
            }

            Console.WriteLine();
            Console.WriteLine("----- A* Search ---------------");
            Search<string> aStarS = new AStarSearch<string>(n1);
            if (aStarS.FindGoal(n2, true))
            {
                OutputSearchResult(aStarS);
                Console.WriteLine("Path : ");
                Console.WriteLine(aStarS.Path);
                Console.WriteLine("---------------------------------------");
            }


            //BoardConfiguration bc2 = new BoardConfiguration(3, "7245 6831 ", "12345678 ", BoardConfiguration.PuzzleHeuristic.UNADMISSIBLE);

            //Node<BoardConfiguration> test2 = new SiblingGeneratingNode<BoardConfiguration>(bc2);
            //Search<BoardConfiguration> testSearch2 = new BestFirstSearch<BoardConfiguration>(test2);
            //testSearch2.FindGoal(new Node<BoardConfiguration>(new BoardConfiguration(3, "12345678 ")), false);
            //Console.WriteLine(testSearch2.GetPathString());

            //foreach(var p in testSearch2.Path)
            //{
            //    Console.WriteLine(p.Destination.HeuristicValue);
            //    Console.WriteLine(p.Destination.Value.ToBoardString());
            //}

            //BoardConfiguration bc = new BoardConfiguration(3, "12384765 ", "1238 4765", BoardConfiguration.PuzzleHeuristic.ADMISSIBLE);

            //Node<BoardConfiguration> test = new SiblingGeneratingNode<BoardConfiguration>(bc);
            //Search<BoardConfiguration> testSearch = new AStarSearch<BoardConfiguration>(test);
            //testSearch.FindGoal(new Node<BoardConfiguration>(new BoardConfiguration(3, "1238 4765")), false);

            //Console.WriteLine(testSearch.GetPathString());

            //Console.WriteLine(testSearch.LastSearchTime.ToString("h\\:m\\.ffff"));

            //Console.WriteLine(bc);
            //Console.WriteLine(bc.MoveBlankLeft());
            //Console.WriteLine(bc.MoveBlankUp());
            //Console.WriteLine(bc.MoveBlankRight());
            //Console.WriteLine(bc.MoveBlankDown());

            //Node<string> A = new FixedHeuristicNode<string>("A", 0);
            //Node<string> B = new FixedHeuristicNode<string>("B", 0);
            //Node<string> C = new FixedHeuristicNode<string>("C", 0);
            //Node<string> D = new FixedHeuristicNode<string>("D", 0);
            //Node<string> E = new FixedHeuristicNode<string>("E", 0);
            //Node<string> F = new FixedHeuristicNode<string>("F", 0);
            //Node<string> G = new FixedHeuristicNode<string>("G", 0);

            //A.AddSiblingUndirected(B, 0);
            //A.AddSiblingUndirected(C, 0);

            //B.AddSiblingUndirected(D, 0);
            //B.AddSiblingUndirected(E, 0);

            //E.AddSiblingUndirected(F, 0);
            //E.AddSiblingUndirected(G, 0);

            //Search<string> s = new BreadthFirstSearch<string>(A);
            //s.FindGoal(G);

            //Node<string> n1 = new FixedHeuristicNode<string>("Arad", 366);
            //Node<string> n2 = new FixedHeuristicNode<string>("Bucharest", 0);
            //Node<string> n3 = new FixedHeuristicNode<string>("Craiova", 160);
            //Node<string> n4 = new FixedHeuristicNode<string>("Dobreta", 366);
            //Node<string> n5 = new FixedHeuristicNode<string>("Fagaras", 176);
            //Node<string> n6 = new FixedHeuristicNode<string>("Oradea", 380);
            //Node<string> n7 = new FixedHeuristicNode<string>("Lugoj", 244);
            //Node<string> n8 = new FixedHeuristicNode<string>("Mehadia", 241);
            //Node<string> n9 = new FixedHeuristicNode<string>("Pitesti", 100);
            //Node<string> n10 = new FixedHeuristicNode<string>("Rimnicu Vilcea", 193);
            //Node<string> n12 = new FixedHeuristicNode<string>("Sibiu", 253);
            //Node<string> n13 = new FixedHeuristicNode<string>("Timisoara", 329);
            //Node<string> n14 = new FixedHeuristicNode<string>("Zerind", 374);

            //n1.AddSiblingUndirected(n13, 118);
            //n1.AddSiblingUndirected(n12, 140);
            //n1.AddSiblingUndirected(n14, 75);

            //n13.AddSiblingUndirected(n7, 111);

            //n7.AddSiblingUndirected(n8, 70);

            //n8.AddSiblingUndirected(n4, 75);

            //n4.AddSiblingUndirected(n3, 120);

            //n3.AddSiblingUndirected(n9, 138);
            //n3.AddSiblingUndirected(n10, 146);

            //n10.AddSiblingUndirected(n9, 97);
            //n10.AddSiblingUndirected(n12, 80);

            //n14.AddSiblingUndirected(n6, 71);

            //n6.AddSiblingUndirected(n12, 151);

            //n12.AddSiblingUndirected(n5, 99);

            //n5.AddSiblingUndirected(n2, 211);

            //n9.AddSiblingUndirected(n2, 101);


            //Search<string> s2 = new DepthFirstSearch<string>(n1);
            //s2.FindGoal(n2,true);

            //Heap<string> h = new Heap<string>();
            //h.AddElement(4, "4");
            //h.AddElement(1, "1");
            //h.AddElement(3, "3");
            //h.AddElement(2, "2");

            //Console.WriteLine(h.RemoveTop());
            //Console.WriteLine(h.RemoveTop());
            //Console.WriteLine(h.RemoveTop());
            //Console.WriteLine(h.RemoveTop());

            //Node S = new Node("S", 0);
            //Node A = new Node("A", 0);
            //Node B = new Node("B", 0);
            //Node C = new Node("C", 0);
            //Node G = new Node("G", 0);

            //S.AddChild(A, 1);
            //S.AddChild(B, 2);
            //A.AddChild(B, 1);
            //A.AddChild(C, 1);
            //B.AddChild(C, 1);
            //C.AddChild(G, 1);

            //Search s = new DepthFirstSearch(S);
            //s.FindGoal(G);

            Console.ReadLine();
        }
    }
}
