﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1.Puzzle
{
    class BoardConfiguration : Graph.IDynamicChildren<BoardConfiguration>, Graph.IDynamicHeuristic
    {
        private struct Position
        {
            private int _x;
            private int _y;

            public int X
            {
                get
                {
                    return _x;
                }
            }

            public int Y
            {
                get
                {
                    return _y;
                }

            }

            public Position(int x, int y)
            {
                _x = x;
                _y = y;
            }
        }
        public enum PuzzleHeuristic
        {
            NONE,
            MISPLACED,
            MANHATTAN,
            MIN_MIS_MAN,
            ADMISSIBLE,
            UNADMISSIBLE
        }

        private char[,] _board;
        private int _board_size;
        private Position _blank_position;
        private string _goal_configuration;
        private PuzzleHeuristic _puzzle_heuristic;

        private BoardConfiguration(int size)
        {
            _board_size = size;
            _board = new char[size, size];
        }

        public BoardConfiguration(int size, string initialConfiguration)
            : this(size)
        {
            SetBoardFromString(initialConfiguration);
        }

        public BoardConfiguration(int size, string initialConfiguration, string goalConfiguration, PuzzleHeuristic h)
            : this(size, initialConfiguration)
        {
            SetBoardFromString(initialConfiguration);
            _goal_configuration = goalConfiguration;
            _puzzle_heuristic = h;
        }

        public BoardConfiguration(BoardConfiguration toCopy)
            : this(toCopy._board_size)
        {
            for (int y = 0; y < _board_size; ++y)
            {
                for (int x = 0; x < _board_size; ++x)
                {
                    _board[x, y] = toCopy._board[x, y];
                }
            }
            _blank_position = toCopy._blank_position;
            _goal_configuration = toCopy._goal_configuration;
            _puzzle_heuristic = toCopy._puzzle_heuristic;
        }

        private void SetBoardFromString(string data)
        {
            for (int y = 0; y < _board_size; ++y)
            {
                for (int x = 0; x < _board_size; ++x)
                {
                    _board[x, y] = data[x + (y * _board_size)];
                    if (_board[x, y] == ' ')
                    {
                        _blank_position = new Position(x, y);
                    }
                }
            }
        }

        private void Swap(Position p1, Position p2)
        {
            char temp = _board[p1.X, p1.Y];
            _board[p1.X, p1.Y] = _board[p2.X, p2.Y];
            _board[p2.X, p2.Y] = temp;
        }

        private void SwapBlank(Position dest)
        {
            Swap(_blank_position, dest);
            _blank_position = dest;
        }

        private void DoMoveLeft()
        {
            Position newPos = new Position(_blank_position.X - 1, _blank_position.Y);
            SwapBlank(newPos);
        }

        private void DoMoveUp()
        {
            Position newPos = new Position(_blank_position.X, _blank_position.Y - 1);
            SwapBlank(newPos);
        }

        private void DoMoveRight()
        {

            Position newPos = new Position(_blank_position.X + 1, _blank_position.Y);
            SwapBlank(newPos);

        }

        private void DoMoveDown()
        {

            Position newPos = new Position(_blank_position.X, _blank_position.Y + 1);
            SwapBlank(newPos);
        }

        private void DoTranspose()
        {

        }

        public BoardConfiguration MoveBlankLeft()
        {
            if (_blank_position.X > 0)
            {
                BoardConfiguration temp = new BoardConfiguration(this);
                temp.DoMoveLeft();
                return temp;
            }
            else
            {
                return null;
            }

        }

        public BoardConfiguration MoveBlankUp()
        {
            if (_blank_position.Y > 0)
            {
                BoardConfiguration temp = new BoardConfiguration(this);
                temp.DoMoveUp();
                return temp;
            }
            else
            {
                return null;
            }

        }

        public BoardConfiguration MoveBlankRight()
        {
            if (_blank_position.X < _board_size - 1)
            {
                BoardConfiguration temp = new BoardConfiguration(this);
                temp.DoMoveRight();
                return temp;
            }
            else
            {
                return null;
            }

        }

        public BoardConfiguration MoveBlankDown()
        {
            if (_blank_position.Y < _board_size - 1)
            {
                BoardConfiguration temp = new BoardConfiguration(this);
                temp.DoMoveDown();
                return temp;
            }
            else
            {
                return null;
            }

        }

        public string GetTransposedString()
        {
            StringBuilder sb = new StringBuilder();
            for (int y = 0; y < _board_size; ++y)
            {
                for (int x = 0; x < _board_size; ++x)
                {
                    sb.Append(_board[y, x]);
                }
            }
            return sb.ToString();
        }

        public string ToBoardString()
        {
            StringBuilder sb = new StringBuilder();
            for (int y = 0; y < _board_size; ++y)
            {
                sb.Append(_board[0, y]).Append("|").Append(_board[1, y]).Append("|").AppendLine(_board[2, y].ToString());
                sb.AppendLine("-----");
                //for (int x = 0; x < _board_size; ++x)
                //{
                //    sb.Append(_board[x, y]);
                //}
            }
            return sb.ToString();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int y = 0; y < _board_size; ++y)
            {
                for (int x = 0; x < _board_size; ++x)
                {
                    sb.Append(_board[x, y]);
                }
            }
            return sb.ToString();
        }


        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj is BoardConfiguration)
            {
                return ToString().Equals(obj.ToString());
            }
            else
            {
                return false;
            }
        }




        public List<BoardConfiguration> GenerateSiblings()
        {
            List<BoardConfiguration> res = new List<BoardConfiguration>();

            BoardConfiguration temp = MoveBlankLeft();
            if (temp != null)
            {
                res.Add(temp);
            }

            temp = MoveBlankUp();
            if (temp != null)
            {
                res.Add(temp);
            }

            temp = MoveBlankRight();
            if (temp != null)
            {
                res.Add(temp);
            }

            temp = MoveBlankDown();
            if (temp != null)
            {
                res.Add(temp);
            }

            return res;
        }

        public int GetHeuristicValue()
        {
            switch (_puzzle_heuristic)
            {
                case PuzzleHeuristic.MISPLACED:
                    return GetMisplacedHeuristicValue();
                case PuzzleHeuristic.MANHATTAN:
                    return GetManhattanHeuristicValue();
                case PuzzleHeuristic.MIN_MIS_MAN:
                    return Math.Min(GetMisplacedHeuristicValue(), GetManhattanHeuristicValue());
                case PuzzleHeuristic.ADMISSIBLE:
                    return GetAdminssibleHeuristic();
                case PuzzleHeuristic.UNADMISSIBLE:
                    return GetUnadmissibleHeuristic() /*Math.Max(GetUnadmissibleHeuristic(), GetMisplacedHeuristicValue())*/;
            }

            return 0;
        }

        private int GetMisplacedHeuristicValue()
        {
            int misplaced = 0;
            string currentDisposition = ToString();
            for (int i = 0; i < currentDisposition.Length; ++i)
            {
                if (currentDisposition[i] != _goal_configuration[i])
                {
                    ++misplaced;
                }
            }
            return misplaced;
        }

        private int GetManhattanHeuristicValue()
        {
            Dictionary<char, Position> currentBoard = new Dictionary<char, Position>();
            Dictionary<char, Position> goalBoard = new Dictionary<char, Position>();
            int distanceSum = 0;

            string currentDisposition = ToString();
            for (int y = 0; y < _board_size; ++y)
            {
                for (int x = 0; x < _board_size; ++x)
                {
                    Position p = new Position(x, y);
                    currentBoard.Add(currentDisposition[x + (y * _board_size)], p);
                    goalBoard.Add(_goal_configuration[x + (y * _board_size)], p);
                }
            }
            for(int i = 0; i < currentDisposition.Length; ++i)
            {
                if(currentDisposition[i] != ' ')
                {
                    Position p1 = currentBoard[currentDisposition[i]];
                    Position p2 = goalBoard[currentDisposition[i]];
                    distanceSum += Math.Abs(p2.X - p1.X) + Math.Abs(p2.Y - p1.Y);
                }
                
            }
            return distanceSum;
        }

        private int GetAdminssibleHeuristic()
        {
            Dictionary<char, Position> currentBoard = new Dictionary<char, Position>();
            Dictionary<char, Position> goalBoard = new Dictionary<char, Position>();
            int distanceSum = 0;

            string currentDisposition = ToString();
            for (int y = 0; y < _board_size; ++y)
            {
                for (int x = 0; x < _board_size; ++x)
                {
                    Position p = new Position(x, y);
                    currentBoard.Add(currentDisposition[x + (y * _board_size)], p);
                    goalBoard.Add(_goal_configuration[x + (y * _board_size)], p);
                }
            }
            for (int i = 0; i < currentDisposition.Length; ++i)
            {
                if (currentDisposition[i] != ' ')
                {
                    Position p1 = currentBoard[currentDisposition[i]];
                    Position p2 = goalBoard[currentDisposition[i]];

                    if (p1.X== p1.Y && p2.X == p2.Y) //Diag. move
                    {
                        distanceSum += Math.Abs(p2.X - p1.X); //If already in right position, should be 0
                    }
                    else if(p1.X == p2.X || p1.Y == p2.Y)//Straight line move
                    {
                        distanceSum += Math.Abs(p2.X - p1.X) + Math.Abs(p2.Y - p1.Y);//should cancel out X or Y since they are on the same axis
                    }
                    else //L-shaped distance
                    {
                        distanceSum += 2;
                    }

                    distanceSum += Math.Abs(p2.X - p1.X) + Math.Abs(p2.Y - p1.Y);
                }

            }
            return distanceSum;
        }

        private int GetUnadmissibleHeuristic()
        {
            int rowColCount = 0;

            string currentRepresentation = ToString();
            string currentTransposedRepresentation = GetTransposedString();
            string goalTransposed = (new BoardConfiguration(_board_size, _goal_configuration)).GetTransposedString();

            rowColCount += GetRowColInPlace(currentRepresentation, _goal_configuration);
            rowColCount += GetRowColInPlace(currentTransposedRepresentation, goalTransposed);
            
            return rowColCount;

        }

        private int GetRowColInPlace(string current, string goal)
        {
            int count = 0;
            for(int i = 0; i < _board_size; ++i)
            {
                if(!current.Substring(i*_board_size,_board_size).Equals(goal.Substring(i * _board_size, _board_size)))
                {
                    ++count;
                }
            }
            return count;
        }
    }
}
