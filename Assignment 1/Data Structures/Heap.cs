﻿using Assignment_1.Graph;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    class Heap<T> : IEnumerable<T>
    {
        private class HeapNode
        {
            private int _key;
            private T _value;

            public int Key
            {
                get
                {
                    return _key;
                }

                set
                {
                    _key = value;
                }
            }

            public T Value
            {
                get
                {
                    return _value;
                }

                set
                {
                    _value = value;
                }
            }
            public HeapNode(int key, T value)
            {
                _key = key;
                _value = value;
            }
        }

        private List<HeapNode> _nodes;
        private int _last_node_index;

        public bool IsEmpty
        {
            get
            {
                return _last_node_index == 0;
            }
        }

        public T CurrentRoot
        {
            get
            {
                if(_last_node_index > 0)
                {
                    return _nodes[1].Value;
                }
                else
                {
                    return default(T);
                }
                
            }
        }

        public Heap()
        {
            _nodes = new List<HeapNode>();
            _nodes.Add(new HeapNode(-1, default(T)));
            _last_node_index = 0;
        }


        private void AdjustHeapUp()
        {
            bool validHeap = false;
            int currentIndex = _last_node_index;
            int parentIndex = currentIndex / 2;
            while (currentIndex != 1 && !validHeap)
            {
                HeapNode current = (HeapNode)_nodes[currentIndex];
                HeapNode parent = (HeapNode)_nodes[parentIndex];
                if (parent.Key > current.Key)
                {
                    _nodes[parentIndex] = current;
                    _nodes[currentIndex] = parent;
                    currentIndex = parentIndex;
                    parentIndex = currentIndex / 2;
                }
                else
                {
                    validHeap = true;
                }
            }
            
        }

        private void AdjustHeapDown()
        {
            int index = 1;
            while(index <= _last_node_index)
            {
                if (index * 2 + 1 <= _last_node_index)
                {
                    if (_nodes[index].Key > _nodes[index * 2].Key)
                    {
                        Swap(index, index * 2);
                        index = index * 2;
                    } else if (_nodes[index].Key > _nodes[index * 2 + 1].Key)
                    {
                        Swap(index, index * 2 + 1);
                        index = index * 2 + 1;
                    }
                    else
                    {
                        break;
                    }
                }
                else if (index * 2 <= _last_node_index)
                {
                    if (_nodes[index].Key > _nodes[index * 2].Key)
                    {
                        Swap(index, index * 2);
                        index = index * 2;
                    }else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
        }

        private void Swap(int index1, int index2)
        {
            HeapNode temp = _nodes[index1];
            _nodes[index1] = _nodes[index2];
            _nodes[index2] = temp;
        }

        public void AddElement(int key, T value)
        {
            _nodes.Add(new HeapNode(key, value));
            ++_last_node_index;
            AdjustHeapUp();
        }

        public T RemoveTop()
        {
            if (!IsEmpty)
            {
                T temp = _nodes[1].Value;
                Swap(1, _last_node_index);
                _nodes.RemoveAt(_last_node_index--);
                AdjustHeapDown();
                return temp;
            }else
            {
                return default(T);
            }
            
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for(int i = 1; i <= _last_node_index; ++i)
            {
                sb.Append(_nodes[i].Value).Append(",");
            }
            if(sb.Length != 0)
            {
                sb.Remove(sb.Length - 1, 1);
            }
            return sb.ToString();
        }

        public IEnumerator<T> GetEnumerator()
        {
            for(int i = 1; i<=_last_node_index; ++i)
            {
                yield return _nodes[i].Value;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
