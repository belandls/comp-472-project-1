﻿using Assignment_1.Graph;
using Nito;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1.Searches
{
    class NodePath<T>
    {
        private Deque<Edge<T>> _path;
        private int _path_cost;

        public int Size
        {
            get
            {
                return _path.Count;
            }
        }

        public int Cost
        {
            get
            {
                return _path_cost;
            }
        }

        public Edge<T> FirstNode
        {
            get
            {
                return _path[0];
            }
        }

        public Edge<T> LastNode
        {
            get
            {
                return _path[_path.Count - 1];
            }
        }

        public NodePath()
        {
            _path = new Deque<Edge<T>>();
            _path_cost = 0;
        }

        public void AddEdge(Edge<T> e)
        {
            _path.AddToFront(e);
            _path_cost += e.Value;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            foreach (var e in _path)
            {
                sb.AppendLine(e.ToString());
            }

            return sb.ToString();
        }

    }
}
