﻿using Assignment_1.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1.Searches
{
    class DepthFirstSearch<T> : DequeBasedSearch<T>
    {
        public DepthFirstSearch(Node<T> root)
            : base(root)
        {

        }

        protected override Edge<T> TakeNextEdge()
        {
            return OpenList.RemoveFromFront();
        }

        protected override void UpdateOpenList(Edge<T> lastEdgeTaken)
        {
            foreach (Graph.Edge<T> e in lastEdgeTaken.Destination.Siblings.AsEnumerable().Reverse())
            {
                if (!ClosedList.Contains(e) && //To avoid loops
                    !ClosedList.Contains(e.GetReverseEdge()) //to avoid going back
                    && !OpenList.Contains(e)) 
                {
                    OpenList.AddToFront(e);
                }
            }
        }
    }
}
