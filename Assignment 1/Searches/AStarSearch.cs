﻿using Assignment_1.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1.Searches
{
    class AStarSearch<T> : HeapBasedSearch<T>
    {
        private HashSet<Edge<T>> _open_list_contains;
        private Dictionary<Node<T>, int> _g_of_n;
        public AStarSearch(Node<T> root) : base(root)
        {
            _open_list_contains = new HashSet<Edge<T>>();
            _g_of_n = new Dictionary<Node<T>, int>();
            _g_of_n.Add(root, 0);
        }

        protected override Edge<T> TakeNextEdge()
        {
            Edge<T> temp = OpenList.RemoveTop();
            _open_list_contains.Remove(temp);
            if (_g_of_n.Count == 0)
            {
                _g_of_n[temp.Destination] = temp.Value;
            }
            else
            {
                _g_of_n[temp.Destination] = temp.Value + _g_of_n[temp.Source];
            }
            
            return temp;
        }

        protected override void UpdateOpenList(Edge<T> lastEdgeTaken)
        {
            foreach (var s in lastEdgeTaken.Destination.Siblings)
            {
                if (!ClosedList.Contains(s) && !_open_list_contains.Contains(s) && !ClosedList.Contains(s.GetReverseEdge()))
                {
                    int costSoFar;
                    if(_g_of_n.Count == 0)
                    {
                        costSoFar = s.Value;
                    }else
                    {
                        costSoFar = _g_of_n[s.Source] + s.Value;
                    }
                    Console.WriteLine(String.Format("{0} : {1}", s.ToString(), s.Destination.HeuristicValue));
                    OpenList.AddElement(s.Destination.HeuristicValue + costSoFar, s);
                    _open_list_contains.Add(s);
                }
            }
        }
    }
}
