﻿using Assignment_1.Graph;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1.Searches
{
    abstract class Search<T>
    {
        private HashSet<Edge<T>> _closed_list;
        private Node<T> _root;
        private TimeSpan _last_search_time;
        private NodePath<T> _path;

        protected HashSet<Edge<T>> ClosedList
        {
            get
            {
                return _closed_list;
            }
        }

        public TimeSpan LastSearchTime
        {
            get
            {
                return _last_search_time;
            }
        }

        public NodePath<T> Path
        {
            get
            {
                return _path;
            }
        }

        protected abstract bool IsOpenListEmpty { get; }

        public Search(Node<T> root)
        {
            _closed_list = new HashSet<Edge<T>>();
            _root = root;
        }

        abstract protected Edge<T> TakeNextEdge();
        abstract protected void UpdateOpenList(Edge<T> lastEdgeTaken);

        public String GetClosedListString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("[");
            foreach (Edge<T> e in _closed_list)
            {
                sb.Append(e.Destination).Append(", ");
            }
            if (_closed_list.Count > 0)
            {
                sb.Remove(sb.Length - 2, 2);
            }
            sb.Append("]");
            return sb.ToString();
        }

        public abstract String GetOpenListString();

        public bool FindGoal(Node<T> g, bool outputLists = true)
        {
            bool searchedEverything = false;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Edge<T> currentEdgeTaken = new Edge<T>(0, null, _root);
            while (!currentEdgeTaken.Destination.Equals(g) && !searchedEverything)
            {
                ClosedList.Add(currentEdgeTaken);
                UpdateOpenList(currentEdgeTaken);
                if (outputLists)
                {
                    Console.WriteLine(String.Format("Closed : {0}, Open : {1}", GetClosedListString(), GetOpenListString()));
                }
                if (IsOpenListEmpty)
                {
                    searchedEverything = true;
                }else
                {
                    currentEdgeTaken = TakeNextEdge();
                }
                
            }
            sw.Stop();
            _last_search_time = sw.Elapsed;
            if (searchedEverything)
            {
                //_path = new List<Edge<T>>();
            }else
            {
                GeneratePathFromLastEdgeTaken(currentEdgeTaken);
            }
            
            return !searchedEverything;
        }

        private void GeneratePathFromLastEdgeTaken(Edge<T> last)
        {
            _path = new NodePath<T>();
            var closedListAsList = _closed_list.ToList();
            _path.AddEdge(last);
            while(last.Source != null)
            {
                last = closedListAsList.First((e) => e.Destination.Equals(last.Source));
                _path.AddEdge(last);
            }
        }

        

    }
}
