﻿using Assignment_1.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1.Searches
{
    abstract class HeapBasedSearch<T> : Search<T>
    {
        private Heap<Edge<T>> _open_list;
        protected override bool IsOpenListEmpty
        {
            get
            {
                return _open_list.IsEmpty;
            }
        }

        protected Heap<Edge<T>> OpenList
        {
            get
            {
                return _open_list;
            }
        }

        public HeapBasedSearch(Node<T> root) : base(root)
        {
            _open_list = new Heap<Edge<T>>();
        }


        public override string GetOpenListString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("[");
            foreach (Edge<T> e in _open_list)
            {
                sb.Append(e.Destination).Append(", ");
            }
            sb.Remove(sb.Length - 2, 2);
            sb.Append("]");
            return sb.ToString();
        }


    }
}
