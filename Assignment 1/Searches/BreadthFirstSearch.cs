﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment_1.Graph;

namespace Assignment_1.Searches
{
    class BreadthFirstSearch<T> : DequeBasedSearch<T>
    {
        public BreadthFirstSearch(Node<T> root) : base(root)
        {
        }

        protected override Edge<T> TakeNextEdge()
        {
            return OpenList.RemoveFromFront();
        }

        protected override void UpdateOpenList(Edge<T> lastEdgeTaken)
        {
            foreach (Graph.Edge<T> e in lastEdgeTaken.Destination.Siblings)
            {
                if (!ClosedList.Contains(e) && !ClosedList.Contains(e.GetReverseEdge()) && !OpenList.Contains(e))
                {
                    OpenList.AddToBack(e);
                }
            }
        }
    }
}
