﻿using Assignment_1.Graph;
using Nito;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1.Searches
{
    abstract class DequeBasedSearch<T> : Search<T>
    {
        private Deque<Edge<T>> _open_list;
        protected override bool IsOpenListEmpty
        {
            get
            {
                return _open_list.Count == 0;
            }
        }

        public override string GetOpenListString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("[");
            foreach (Edge<T> e in _open_list)
            {
                sb.Append(e.Destination).Append(", ");
            }
            if(sb.Length > 1)
            {
                sb.Remove(sb.Length - 2, 2);
            }
            
            sb.Append("]");
            return sb.ToString();
        }

        protected Deque<Edge<T>> OpenList
        {
            get
            {
                return _open_list;
            }
        }

        public DequeBasedSearch(Node<T> root) : base(root)
        {
            _open_list = new Deque<Edge<T>>();
        }

    }
}
