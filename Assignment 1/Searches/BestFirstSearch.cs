﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment_1.Graph;

namespace Assignment_1.Searches
{
    class BestFirstSearch<T> : HeapBasedSearch<T>
    {
        private HashSet<Edge<T>> _open_list_contains;
        public BestFirstSearch(Node<T> root) : base(root)
        {
            _open_list_contains = new HashSet<Edge<T>>();
        }

        protected override Edge<T> TakeNextEdge()
        {
            Edge<T> temp = OpenList.RemoveTop();
            _open_list_contains.Remove(temp);
            return temp;
        }

        protected override void UpdateOpenList(Edge<T> lastEdgeTaken)
        {
            foreach(var s in lastEdgeTaken.Destination.Siblings)
            {
                if(!ClosedList.Contains(s) && !_open_list_contains.Contains(s) && !ClosedList.Contains(s.GetReverseEdge()))
                {
                    OpenList.AddElement(s.Destination.HeuristicValue, s);
                    _open_list_contains.Add(s);
                }
            }
        }
    }
}
