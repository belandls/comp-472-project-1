﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1.Graph
{
    class FixedHeuristicNode<T> : Node<T>
    {
        private int _heuristic;

        public override int HeuristicValue
        {
            get
            {
                return _heuristic;
            }
        }

        public FixedHeuristicNode(T value, int heuristic) : base(value)
        {
            _heuristic = heuristic;
        }

        
    }
}
