﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1.Graph
{
    public interface IDynamicChildren<T>
    {
        List<T> GenerateSiblings();
    }
}
