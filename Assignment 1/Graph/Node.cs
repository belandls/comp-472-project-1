﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1.Graph
{
    public class Node<T>
    {
        private T _value;
        private List<Edge<T>> _siblings;

        public virtual int HeuristicValue {
            get
            {
                return 0;
            } 
        }

        public virtual List<Edge<T>> Siblings
        {
            get
            {
                return _siblings;
            }
        }

        public T Value
        {
            get
            {
                return _value;
            }
        }

        public Node(T value)
        {
            _value = value;
            _siblings = new List<Edge<T>>();
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj is Node<T>)
            {
                return ((Node<T>)obj)._value.Equals(_value); 
            }
            else
            {
                return false;
            }
        }

        public void AddSiblingUndirected(Node<T> s, int cost)
        {
            _siblings.Add(new Edge<T>(cost,this, s));
            s._siblings.Add(new Edge<T>(cost, s, this));
        }
        public void AddSiblingDirected(Node<T> s, int cost)
        {
            _siblings.Add(new Edge<T>(cost, this, s));
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }
        public override string ToString()
        {
            return _value.ToString();
        }
    }
}
