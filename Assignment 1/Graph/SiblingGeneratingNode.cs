﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1.Graph
{
    class SiblingGeneratingNode<T> : Node<T> where T:IDynamicChildren<T>,IDynamicHeuristic
    {
        private bool _sibling_generated;
        private int _generated_heuristic;

        public override int HeuristicValue
        {
            get
            {
                return _generated_heuristic;
            }
        }

        public override List<Edge<T>> Siblings
        {
            get 
            {
                if (!_sibling_generated)
                {
                    var temp = Value.GenerateSiblings();
                    foreach (var s in temp)
                    {
                        AddSiblingDirected(new SiblingGeneratingNode<T>(s), 1);
                    }
                    _sibling_generated = true;
                }
                return base.Siblings;
            }
        }

        public SiblingGeneratingNode(T value) : base(value)
        {
            _generated_heuristic = value.GetHeuristicValue();
            _sibling_generated = false;
        }
    }
}
