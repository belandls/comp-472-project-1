﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1.Graph
{
    public class Edge<T>
    {
        private int _value;
        private Node<T> _destination;
        private Node<T> _source;

        internal Node<T> Destination
        {
            get
            {
                return _destination;
            }

        }

        public Node<T> Source
        {
            get
            {
                return _source;
            }
        }

        public int Value
        {
            get
            {
                return _value;
            }
        }

        public Edge(int value, Node<T> src, Node<T> dest)
        {
            _value = value;
            _source = src;
            _destination = dest;
        }

        public Edge<T> GetReverseEdge()
        {
            return new Edge<T>(_value, _destination, _source);
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj is Edge<T>)
            {
                Edge<T> cObj = (Edge<T>)obj;
                if(_source == null)
                {
                    if(cObj._source == null)
                    {
                        return (_value == Value) && (_destination.Equals(cObj._destination));
                    }else
                    {
                        return false;
                    }
                }else
                {
                    return (_value == Value) && (_destination.Equals(cObj._destination)) && (_source.Equals(cObj._source));
                }
            }
            else
            {
                return false;
            }
        }

        public override string ToString()
        {
            return String.Format("{0}-{1}->{2}", _source, _value, _destination);
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }
    }
}
